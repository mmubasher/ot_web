/* eslint-disable no-unused-vars */
// Make a copy of this file and save it as config.js (in the js directory).

// Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
// Do not include the trailing slash. See the README for more information:

var SAMPLE_SERVER_BASE_URL = 'http://YOUR-SERVER-URL';

// OR, if you have not set up a web server that runs the learning-opentok-php code,
// set these values to OpenTok API key, a valid session ID, and a token for the session.
// For test purposes, you can obtain these from https://tokbox.com/account.

var API_KEY = '46099392';
var SESSION_ID = '1_MX40NjA5OTM5Mn5-MTU0NDE4MDg4Nzc5OX43cnY3REpuMW1hMGFuUWRpNTdzeU03QkF-fg';
var TOKEN = 'T1==cGFydG5lcl9pZD00NjA5OTM5MiZzaWc9ZWE1ZDJkZmQ3ZGM2MTkwNTE3MGE2ODdiOTU4ZDM4NWNmNDQ1NzE0ZjpzZXNzaW9uX2lkPTFfTVg0ME5qQTVPVE01TW41LU1UVTBOREU0TURnNE56YzVPWDQzY25ZM1JFcHVNVzFoTUdGdVVXUnBOVGR6ZVUwM1FrRi1mZyZjcmVhdGVfdGltZT0xNTQ0MTgwOTA3Jm5vbmNlPTAuMDE2MjcyODY5MzA4MjAzMDQyJnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE1NDY3NzI5MDgmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=';
